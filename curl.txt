curl -i -H "content-type: application/json" -X POST
'{"title": "prueba 1", "description": "creada para prueba",
"deadline": "2020-12-12 12:00:00"}'
http://127.0.0.1:5000/api/v1/tasks

Obtener listado de tareas

curl -i -H "Content-type: application/json" http://127.0.0.1:5000/api/v1/tasks

ordenamiento
curl -i -H "Content-type: application/json" http://127.0.0.1:5000/api/v1/tasks?order=asc

paginacion
curl -i -H "Content-type: application/json" http://127.0.0.1:5000/api/v1/tasks?order=asc&page=1

obtener tarea con id = 1
curl -i -H "Content-type: application/json" http://127.0.0.1:5000/api/v1/tasks/1

crear tarea
curl -i -H "Content-type: application/json" -X POST -d '{"title": "Nueva tarea", "description":
"Nueva description", "deadline": "2019-12-12 12:00:00"}' http://127.0.0.1:5000/api/v1/tasks

actualizar tarea
curl -i -H "Content-type: application/json" -X PUT -d '{"title": "Nueva descripción"}' http://127.0.0.1:5000/api/v1/tasks/1

eliminar tarea
curl -i -H "Content-type: application/json" -X DELETE http://127.0.0.1:5000/api/v1/tasks/1
